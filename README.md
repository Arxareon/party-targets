# ⚠ Party Targets has moved to [GitHub](https://github.com/Arxareon/PartyTargets). Look for new updates there!
If you wish to support the future development of Party Targets, you can now [Sponsor](https://github.com/sponsors/Arxareon) my work on GitHub as an option.

***Thank you for understanding!***

- - -
***A simple solution to a simple problem:*** Party Targets shows you who each of your party or raid members are targeting at all times.

## Features
* See who your party and raid members are targeting, and what health are they at - useful in Arena!

## Other Addons
*If you like minimalistic solutions, you might want to check out there Addons as well:*

[**Movement Speed**](https://bitbucket.org/Arxareon/movement-speed) • View the movement speed of anyone in customizable displays and tooltips with fast updates.

[**Remaining XP**](https://bitbucket.org/Arxareon/remaining-xp) • Shows the amount of XP needed to reach the next level & much more.

*Coming soon™:*
[**RP Keyboard**](https://bitbucket.org/Arxareon/rp-keyboard) • Type fantasy letters & runes in chat to talk to other RP Keyboard users.

## Support
*Possibilities to directly support the development of Party Targets and other addons:*

[![Donate on PayPal](https://upload.wikimedia.org/wikipedia/commons/thumb/b/b5/PayPal.svg/124px-PayPal.svg.png)](https://www.paypal.com/donate/?hosted_button_id=Z4FSAFKA5LX98)

## Links
[**CurseForge**](https://www.curseforge.com/wow/addons/party-targets-who-are-they-targeting) • Should you prefer, support development by watching ads on CurseForge, and keep up to date via automatic updates via the CurseForge App.

[**Wago**](https://addons.wago.io/addons/party-targets) • You can also use Wago to get updates and support development by watching ads, and provide much appreciated continuous support via a Subscription.

*You can also get updates via **WoWUP** if you have Wago.io enabled as an addon provider!*

## License
All Rights Reserved unless otherwise explicitly stated.

- - -
Thank you for checking out Party Targets!
If you have any comments, wishes or problems, please, don't be afraid to share them. GLHF!

*Arxareon*